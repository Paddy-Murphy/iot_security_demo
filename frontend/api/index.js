'use strict';

var express = require('express');
var seneca = require('seneca')();
var app = express();
var http = require('http').Server(app);
var webStream = require('./webStream')(http);
var moment = require('moment');
var _ = require('lodash');

var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');

var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var configDB = require('./config/database.js');

// connect to database
mongoose.connect(configDB.url);

// passport configuration
require('./config/passport')(passport); 

app.use(morgan('dev')); // log requests to console
app.use(cookieParser()); // read cookies for auth
app.use(bodyParser.json()); // get info from html forms
app.use(bodyParser.urlencoded({ extended: false }));

// use ejs templating
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

// require for passport
app.use(session({ 
  secret: 'ilovescotchscotchyscotchscotch',
  resave: false,
  saveUninitialized: false
})); 
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

seneca.client({host: process.env.serializer_HOST, port: process.env.serializer_PORT, pin: {role: 'serialize', cmd: 'read'}});
seneca.client({host: process.env.actuator_HOST, port: process.env.actuator_PORT, pin: {role: 'actuate', cmd: 'set'}});

app.use('/', express.static(__dirname + '/../public'));

require('./routes.js')(app, passport);

app.get('/set', function (req, res) {
  seneca.act({role: 'actuate', cmd: 'set', offset: req.query.offset}, function(err) {
    if (err) {
      res.json({result: err});
    }
    else {
      res.json({result: 'ok'});
    }
    res.end();
  });
});



var lastEmitted = 0;
setInterval(function() {
  seneca.act({role: 'serialize', cmd: 'read', sensorId: '1', start: moment().subtract(10, 'minutes').utc().format(), end: moment().utc().format()}, function(err, data) {
    var toEmit = [];

    _.each(data[0], function(point) {
      if (moment(point.time).unix() > lastEmitted) {
        lastEmitted = moment(point.time).unix();
        point.time = (new Date(point.time)).getTime();
        toEmit.push(point);
      }
    });
    if (toEmit.length > 0) {
      console.log('will emit');
      console.log(toEmit);
      webStream.emit(toEmit);
    }
    else {
      console.log('no emit');
    }
  });
}, 1000);



http.listen(3000, function(){
  console.log('listening on *:3000');
});

