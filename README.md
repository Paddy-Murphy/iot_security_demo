# README #

###Containerised microservices from nearForm Workshop: Developing Microservices
[Workshop](https://github.com/nearform/developing-microservices) creates an app that displays realtime data from a dummy sensor in graph format. 


###Containers include:
**Frontend:**
Web app that displays realtime data from temperature sensor.  
**Actuator:**
Reads sensor data  based on an offset.    
**Sensor:**
Dummy temperature sensor.  
**Serialiser:**
Handles reads & writes to database; uses websocket-stream to update web app.  
**Broker:**
MQTT broker which wires up actuator, seraliser, and sensor.  
**Influx:**
Database used be the seraliser to store temperature data.

####To run:  
Download the following docker images:  
node  
tutum/influxdb


````
$ docker-compose build
$ docker-compose up
````
or to run in background

````
$ docker-compose up -d
````
Point web browser to:
 
````
<docker ip>:3000
````
A graph receiving & displaying realtime temperature data should be displayed.